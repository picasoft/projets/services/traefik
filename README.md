## Traefik

Ce dossier contient les ressources nécessaires pour lancer Traefik, un reverse proxy web et TCP adapté pour Docker.

C'est la pièce la plus importante de l'infrastructure, puisque l'ensemble des communications chiffrées passent (ou passeront à terme) par Traefik.

Ce service doit être lancé sur l'ensemble des machines de l'infrastructure.

Les explications sont sur le Wiki et doivent être modifiées en cas de changement majeur de configuration : https://wiki.picasoft.net/doku.php?id=technique:tech_team:traefik

### Configuration

La configuration a lieu dans les fichiers [traefik.yaml](./traefik.yaml) et [traefik_dynamic.yaml](./traefik_dynamic.yaml).
Notez que toute modification dans ce fichier impactera l'ensemble des machines, puisque le même fichier est utilisé pour l'ensemble des machines.

À des fins de tests, il peut être modifié localement sur les machines, mais doit toujours rester synchronisé avec ce dépôt à long terme.

Pour la génération des certificats, Traefik utilise Let's Encrypt. Il n'y a aucune configuration à faire de ce côté. Attention, le nombre de certificats générables est limité à 50 par semaine.

Si on lance plein de conteneurs de tests, on utilisera temporairement [l'environnement de qualification](https://letsencrypt.org/fr/docs/staging-environment/) de Let's Encrypt, en ajoutant la directive `caServer = "https://acme-staging-v02.api.letsencrypt.org/directory"` sous la section `[certificatesResolvers.letsencrypt.acme]`.

### Lancement

Créer manuellement un réseau pour Traefik : `docker network create proxy`.

#### Certs

Au premier lancement, assurez-vous que :

- Le dossier `/DATA/docker/traefik/certs` existe
- Créez un fichier `acme.json` à l'intérieur
- Changez son propriétaire à `root`
- Changez ses permissions à `600`
- Le fichier contient la chaîne `{}`.

C'est dans ce fichier que seront conservés tous les certificats générés par Traefik.

#### Authentification client

Actuellement utilisé pour la sécurisation du serveur de backups Restic, on utilise des certificats clients pour protéger l'accès des backups.

Il faut générer la Certficate Authority avec le fichier `./gen_ca.sh`.

Quand on a besoin d'un (ou plusieurs) certificat·s client, on le·s génère avec `./gen_clients.sh <NOM> [ <NOM> ... ]`.
Le fichier regroupant le certificat client et sa clé est généré ici : `./ca/<NOM>/bundle.pem`

#### Métriques et dashboard

Pour le bon fonctionnement des métriques, il faut aussi créer un fichier `.env` (dans le même dossier que le Docker Compose) qui devra contenir 2 variables :

- `SERVER_NAME` qui correspond au nom du serveur, par exemple `SERVER_NAME=pica01-test.picasoft.net`
- `TRAEFIK_METRICS_AUTH` qui correspond à la chaîne d'identification htpasswd utilisée pour authentifier sur l'endpoint des métriques, par exemple `TRAEFIK_METRICS_AUTH="traefik:$apr1$bXnknJ0S$GsC.ozNJc/dAkh9uH7Qlg."`
- `TRAEFIK_API_AUTH`, même idée mais pour le [dashboard](https://doc.traefik.io/traefik/operations/dashboard).

### Mise à jour

Il suffit de mettre à jour le tag de l'image dans Compose.
Lire la documentation [sur les mises à jour mineures](https://docs.traefik.io/v2.2/migration/v2/) pour voir s'il y a des opérations à effectuer ou des options dépréciées.

La mise à jour vers Traefik v2 a été effectuée ; quelques détails sont à consulter [sur le wiki](https://wiki.picasoft.net/doku.php?id=technique:adminsys:migration-traefik-v2).
