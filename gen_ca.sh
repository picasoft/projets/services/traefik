#!/bin/bash

# Generate root authority and certificate authority

mkdir -p ./ca

if test -e ./ca/ca.rsa
then
  echo "certificate authority already exists"
  exit 1
fi

openssl genrsa --out ./ca/ca.rsa 4096
openssl req -new -x509 -key ./ca/ca.rsa -sha256 -out ./ca/ca.pem -config gen_ca.cnf -days $((365 * 1000))

