# Generate clients certificates specified on CLI

if test -z "$1"
then
  echo "no client name specified on the command line"
fi

while test -n "$1"
do
  if test -d ./ca/"$1"
  then
    echo "client '$1' already exists, skipping"
    shift
    continue
  fi

  DIR=./ca/"$1"

  mkdir $DIR

  openssl genrsa -out "$DIR"/key.pem 4096
  openssl req -subj "/CN=$1" -new -key "$DIR"/key.pem -out "$DIR"/req.csr
  openssl x509 -req -days $(( 365 * 1000 )) -sha256 -in "$DIR"/req.csr -CA ./ca/ca.pem -CAkey ./ca/ca.rsa -CAcreateserial -out "$DIR"/cert.pem -extfile ./gen_clients.cnf
  cat "$DIR/cert.pem" "$DIR/key.pem" > "$DIR/bundle.pem"

  rm "$DIR/req.csr"

  shift
done
